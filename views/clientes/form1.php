<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Clientes;
?>
<?php $form= ActiveForm::begin(); ?>
<?= $form->field($model, 'NombreCliente')
        ->dropDownList(ArrayHelper::map(Clientes::find()->all(),'CodigoCliente', 'NombreCliente'),
                ['prompt'=>'Elige Cliente',
                 'onchange'=>'$.post("index.php?r=clientes/listarciudad&id='.'"+$(this).val(),function(data){
                                $("select#clientes-ciudad").html(data);
                             });
                             $.post("index.php?r=clientes/contacto&id='.'"+$(this).val(),function(data){
                                $(".contacto-text").val(data);
                            });'
            
                 ]); ?>
<?= $form->field($model, 'Ciudad')
        ->dropDownList(ArrayHelper::map(Clientes::find()->all(),'CodigoCliente', 'Ciudad'),
                ['prompt'=>'Elige Ciudad',
                 'onchange'=>'$.post("index.php?r=clientes/listarcliente&ciudad='.'"+$("select#clientes-ciudad :selected").text(),function(data){
                                $("select#clientes-nombrecliente").html(data);
                            });'
                ]);?>

Contacto: <?= Html::input('text','contacto',"",['class'=>'contacto-text']) ?>
<?php ActiveForm::end(); ?>
<script>

</script>