<?php

namespace app\controllers;

use Yii;
use app\models\Clientes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ClientesController implements the CRUD actions for Clientes model.
 */
class ClientesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Clientes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Clientes::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionForm1(){
        $model=new Clientes();
        return $this->render('form1',['model'=>$model]);
    }
    
    //Devuelve a una consulta Ajax de Jquey que crea la lista con la ciudad a la que pertenece el cliente
    public function actionListarciudad($id){
        $consultas= Clientes::find()->where(['CodigoCliente'=>$id])->all();
        foreach($consultas as $consulta){
            echo '<option value="'.$consulta->CodigoCliente.'">'.$consulta->Ciudad.'</option>';
           
        }
    }
    
    //Devuelve a una consulta Ajax de Jquey el Nombre y Apellidos de contacto del cliente seleccionado
    public function actionContacto($id){
        $consultas=Clientes::find()->where(['CodigoCliente'=>$id])->all();
        foreach($consultas as $consulta){
            echo $consulta->NombreContacto." ".$consulta->ApellidoContacto;
        }
    }
    
    //Devuelve a la consulta Ajax de Jquery una lsita que crea la lista con los clientes de la ciudad seleccionada
    public function actionListarcliente($ciudad){
        $registros= Clientes::find()->where(['Ciudad'=>$ciudad])->all();
        foreach ($registros as $registro){
            echo '<option value="'.$registro->CodigoCliente.'">'.$registro->NombreCliente.'</option>';
        }
        
    }


        /**
     * Displays a single Clientes model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Clientes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Clientes();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->CodigoCliente]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Clientes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->CodigoCliente]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Clientes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Clientes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Clientes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Clientes::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
