<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property int $CodigoCliente
 * @property string $NombreCliente
 * @property string $NombreContacto
 * @property string $ApellidoContacto
 * @property string $Telefono
 * @property string $Fax
 * @property string $LineaDireccion1
 * @property string $LineaDireccion2
 * @property string $Ciudad
 * @property string $Region
 * @property string $Pais
 * @property string $CodigoPostal
 * @property int $CodigoEmpleadoRepVentas
 * @property string $LimiteCredito
 *
 * @property Empleados $codigoEmpleadoRepVentas
 * @property Pagos[] $pagos
 * @property Pedidos[] $pedidos
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CodigoCliente', 'NombreCliente', 'Telefono', 'Fax', 'LineaDireccion1', 'Ciudad'], 'required'],
            [['CodigoCliente', 'CodigoEmpleadoRepVentas'], 'integer'],
            [['LimiteCredito'], 'number'],
            [['NombreCliente', 'LineaDireccion1', 'LineaDireccion2', 'Ciudad', 'Region', 'Pais'], 'string', 'max' => 50],
            [['NombreContacto', 'ApellidoContacto'], 'string', 'max' => 30],
            [['Telefono', 'Fax'], 'string', 'max' => 15],
            [['CodigoPostal'], 'string', 'max' => 10],
            [['CodigoCliente'], 'unique'],
            [['CodigoEmpleadoRepVentas'], 'exist', 'skipOnError' => true, 'targetClass' => Empleados::className(), 'targetAttribute' => ['CodigoEmpleadoRepVentas' => 'CodigoEmpleado']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CodigoCliente' => 'Codigo Cliente',
            'NombreCliente' => 'Nombre Cliente',
            'NombreContacto' => 'Nombre Contacto',
            'ApellidoContacto' => 'Apellido Contacto',
            'Telefono' => 'Telefono',
            'Fax' => 'Fax',
            'LineaDireccion1' => 'Linea Direccion1',
            'LineaDireccion2' => 'Linea Direccion2',
            'Ciudad' => 'Ciudad',
            'Region' => 'Region',
            'Pais' => 'Pais',
            'CodigoPostal' => 'Codigo Postal',
            'CodigoEmpleadoRepVentas' => 'Codigo Empleado Rep Ventas',
            'LimiteCredito' => 'Limite Credito',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEmpleadoRepVentas()
    {
        return $this->hasOne(Empleados::className(), ['CodigoEmpleado' => 'CodigoEmpleadoRepVentas']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPagos()
    {
        return $this->hasMany(Pagos::className(), ['CodigoCliente' => 'CodigoCliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidos()
    {
        return $this->hasMany(Pedidos::className(), ['CodigoCliente' => 'CodigoCliente']);
    }
}
