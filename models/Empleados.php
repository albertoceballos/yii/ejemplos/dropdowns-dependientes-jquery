<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empleados".
 *
 * @property int $CodigoEmpleado
 * @property string $Nombre
 * @property string $Apellido1
 * @property string $Apellido2
 * @property string $Extension
 * @property string $Email
 * @property string $CodigoOficina
 * @property int $CodigoJefe
 * @property string $Puesto
 *
 * @property Clientes[] $clientes
 * @property Empleados $codigoJefe
 * @property Empleados[] $empleados
 * @property Oficinas $codigoOficina
 */
class Empleados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empleados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CodigoEmpleado', 'Nombre', 'Apellido1', 'Extension', 'Email', 'CodigoOficina'], 'required'],
            [['CodigoEmpleado', 'CodigoJefe'], 'integer'],
            [['Nombre', 'Apellido1', 'Apellido2', 'Puesto'], 'string', 'max' => 50],
            [['Extension', 'CodigoOficina'], 'string', 'max' => 10],
            [['Email'], 'string', 'max' => 100],
            [['CodigoEmpleado'], 'unique'],
            [['CodigoJefe'], 'exist', 'skipOnError' => true, 'targetClass' => Empleados::className(), 'targetAttribute' => ['CodigoJefe' => 'CodigoEmpleado']],
            [['CodigoOficina'], 'exist', 'skipOnError' => true, 'targetClass' => Oficinas::className(), 'targetAttribute' => ['CodigoOficina' => 'CodigoOficina']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CodigoEmpleado' => 'Codigo Empleado',
            'Nombre' => 'Nombre',
            'Apellido1' => 'Apellido1',
            'Apellido2' => 'Apellido2',
            'Extension' => 'Extension',
            'Email' => 'Email',
            'CodigoOficina' => 'Codigo Oficina',
            'CodigoJefe' => 'Codigo Jefe',
            'Puesto' => 'Puesto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientes()
    {
        return $this->hasMany(Clientes::className(), ['CodigoEmpleadoRepVentas' => 'CodigoEmpleado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoJefe()
    {
        return $this->hasOne(Empleados::className(), ['CodigoEmpleado' => 'CodigoJefe']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleados()
    {
        return $this->hasMany(Empleados::className(), ['CodigoJefe' => 'CodigoEmpleado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoOficina()
    {
        return $this->hasOne(Oficinas::className(), ['CodigoOficina' => 'CodigoOficina']);
    }
}
